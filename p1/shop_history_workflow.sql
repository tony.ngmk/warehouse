
WITH fbs_shops AS (
    /*all wh related shops will have fbs_tag = 1*/
    SELECT
        DISTINCT a.shop_id,
        b.whs_id,
        fbs_tag,
        fbs_status
    FROM
        sbs_mart.shopee_scbs_db__shop_tab__reg_daily_s0_live a
        INNER JOIN sbs_mart.shopee_scbs_db__shop_whs_tab__reg_daily_s0_live b ON a.shop_id = b.shop_id
    WHERE
        cb_option = 1
        and a.shop_id NOT IN (426379311,445279067,426377685,445275287,446091597,446089250,538890515,558513120)
),
pure_whs_shops AS (
    /*entity_id for pure wh shops*/
    /*During migration, all whs shops has the tag but the fbs_tag is not finished for them*/
    SELECT
        entity_id AS shop_id
    FROM
        marketplace.shopee_tag_attr_v2_db__entity_tag_tab__reg_continuous_s0_live
    WHERE
        grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
        AND tag_id IN (
            --the existing tags to be migrated to 89 series
            9019244283617387951,
            9019244282760330013,
            9019244283388349552,
            9019244282664454760,
            9019244283297794666,
            9019244283205942297,
            9019244283472640019,
            9019244282760330013,
            9019244282760330013,
            -- Mingyu: add MY NSS tag
            --the below are updated tags 
            8929736573703975738,
            8929736572833440241,
            8929736572936569271,
            8929736573202302164,
            8929736572752369013,
            --VN including the NNVN
            8929736573115783069,
            8929736573310682481,
            8858627938927602232
        ) --MX
        /*1 - enabled, 2 - tagged and then removed*/
        AND STATUS = 1
        and entity_id NOT IN (426379311,445279067,426377685,445275287,446091597,446089250,538890515,558513120)
),
tagged_shops AS (
    /*CBWH shops only - with pff tag*/
    SELECT
        shop_id,
        warehouse_id,
        /*logic changed from the old version*/
        max(fbs_tag) AS fbs_tag,
        max(fbs_status) AS fbs_status,
        min(pff_tag) AS pff_tag
    FROM
        (
            SELECT
                n.shop_id,
                o.whs_id AS warehouse_id,
                0 AS fbs_tag,
                0 AS fbs_status,
                0 AS pff_tag
            FROM
                pure_whs_shops n
                INNER JOIN fbs_shops o ON n.shop_id = o.shop_id
            UNION
            SELECT
                shop_id,
                whs_id AS warehouse_id,
                fbs_tag,
                fbs_status,
                1 AS pff_tag
            FROM
                fbs_shops
        ) t
    GROUP BY
        1,
        2
),
shop AS (
    SELECT
        DISTINCT grass_region,
        shop_id,
        STATUS,
        is_holiday_mode
    FROM
        mp_user.dim_shop__reg_s0_live
    WHERE
        tz_type = 'local'
        AND grass_date = ${nexus_data_date}
        AND grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
        AND is_cb_shop = 1
        and shop_id NOT IN (426379311,445279067,426377685,445275287,446091597,446089250,538890515,558513120)
        
),
user AS (
    -- limit data size before join
    SELECT
        shop_id,
        user_name AS username,
        STATUS,
        last_login_datetime,
        registration_datetime
    FROM
        mp_user.dim_user__reg_s0_live
    WHERE
        grass_date = ${nexus_data_date}
        AND tz_type = 'local'
        AND grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
        AND is_cb_shop = 1
        and shop_id NOT IN (426379311,445279067,426377685,445275287,446091597,446089250,538890515,558513120)
),
-- user_account
-- AS (
-- 	SELECT userid,
-- 		DATE (
-- 			from_unixtime(CASE
-- 					WHEN last_login > web_last_login
-- 						THEN last_login
-- 					ELSE web_last_login
-- 					END)
-- 			) AS last_login
-- 	FROM shopee.shopee_account_info_db__account_info_v2_tab
-- 	WHERE grass_schema != ''
-- 		AND grass_sharding != ''
-- 	),
-- user_account AS (
--     SELECT
--         'user_mart_dws_user_login_td_account_info' AS source,
--         user_id AS userid,
--         from_unixtime(last_login_timestamp_td) AS last_login
--     FROM
--         mp_user.dws_user_login_td_account_info__reg_s0_live
--     WHERE
--         tz_type = 'local'
--         AND grass_date = ${nexus_data_date}
--         AND grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
-- ),
seller_index AS (
    -- limit data size before join
    SELECT
        child_shopid,
        ggp_account_name,
        ggp_account_owner,
        gp_account_name,
        gp_account_owner,
        child_account_owner
    FROM
    cncbbi_general.shopee_cb_seller_profile_with_old_column
        --regbd_sf.cb__seller_index_tab
    WHERE
        1=1
        and (grass_region != ''  OR grass_region IS NULL)
        and cast(child_shopid as bigint) Not IN (426379311,445279067,426377685,445275287,446091597,446089250,538890515,558513120)

),


item_stock AS (
    -- limit data size before join
    SELECT
        shop_id,
        shop_level1_category,
        active_item_with_stock_cnt
    FROM
        mp_item.dws_shop_listing_td__reg_s0_live
    WHERE
        grass_date = ${nexus_data_date}
        AND tz_type = 'local'
        AND grass_region IN ('ID', 'PH', 'MY', 'TH', 'SG', 'MX', 'VN')
        and shop_id NOT IN (426379311,445279067,426377685,445275287,446091597,446089250,538890515,558513120)
)
SELECT
    s.grass_region,
    ${nexus_data_date} AS grass_date,
    cast(s.shop_id AS BIGINT) AS shop_id,
    ts.warehouse_id,
    u.username,
    coalesce(x.ggp_account_name, x.gp_account_name) AS ggp_account_name,
    coalesce(x.ggp_account_owner, x.gp_account_owner) AS ggp_account_owner,
    u.STATUS AS user_status,
    s.STATUS AS shop_status,
    cast(u.registration_datetime AS TIMESTAMP) AS registration_time,
    --cast(account.last_login AS TIMESTAMP) AS last_login,
    cast(s.is_holiday_mode AS boolean) AS holiday_mode_on,
    l.active_item_with_stock_cnt,
    CASE
        WHEN u.STATUS = 1
        AND s.STATUS = 1 -- AND cast(account.last_login AS TIMESTAMP) >= ${nexus_data_date} - INTERVAL '7' DAY
        AND (
            s.is_holiday_mode = 0
            OR s.is_holiday_mode IS NULL
        )
        AND l.active_item_with_stock_cnt > 0 THEN 1
        ELSE 0
    END AS is_live_shop,
    -- CASE
    --     WHEN u.status = 1
    --     AND s.status = 1
    --     AND cast(u.last_login_datetime AS timestamp) >= ${nexus_data_date} - INTERVAL '7' DAY
    --     AND (
    --         s.is_holiday_mode = 0
    --         OR s.is_holiday_mode IS NULL
    --     )
    --     AND l.active_item_with_stock_cnt > 0
    --     AND ws.live_whs_shop = 1 THEN 1
    --     ELSE 0
    -- END AS is_live_whs_shop,
    ts.fbs_tag,
    ts.fbs_status,
    ts.pff_tag
FROM
    tagged_shops ts
    INNER JOIN shop s ON ts.shop_id = s.shop_id
    INNER JOIN user AS u ON ts.shop_id = u.shop_id
    INNER JOIN item_stock l ON ts.shop_id = l.shop_id
    --LEFT JOIN user_account account ON u.shop_id = account.userid
    LEFT JOIN seller_index x ON ts.shop_id = cast(x.child_shopid AS BIGINT) -- LEFT JOIN whs_sku_shops ws ON ts.shop_id = ws.shop_id 
)