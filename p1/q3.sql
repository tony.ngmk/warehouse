insert into nexus.test_8251d89c34b13d2c8a395a4d5922b393e4f146473f8888e575f82870a991d5c6_7cca3a2b9ef6f407d76f00b0cf8d0743 /**********************************
  Name: shop info
  Function: 
  Notes: 
  
  Date Developer Activity
  2022-05-12  Hannah  add shops having stock_on_hand > 0
  2022-07-04  Hannah  comment out team_group query, and use new smt logic
  
  **********************************/
 

 WITH fbs_order AS (
  -- all cb shops have orders fulfilled by shopee last month (M-1 to D-1)
  SELECT
  grass_region,
  shop_id,
  /*W-1 Orders as Reference*/
  sum(order_fraction) filter (
  WHERE
  --Hannah: change the sorting using MTD FBS ADO
  grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY)
  --current_date - INTERVAL '7' DAY
  AND current_date - INTERVAL '1' DAY
  ) AS ord
  FROM
  mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
  WHERE
  grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','VN','MX')
  AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
  AND tz_type = 'local'
  AND grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY)
  AND current_date - INTERVAL '1' DAY
  AND is_placed = 1
  AND is_cb_shop = 1
  GROUP BY
  1,
  2
  union 
 

  select 
  Distinct 
  grass_region, shopid as shop_id,
    0 as ord
  from  
  (select grass_region,
  shopid, sum(stock_on_hand) sellable_stock
  from sbs_mart.shopee_bd_sbs_mart_v2
  where grass_date = current_date - INTERVAL '1' DAY
  and is_cb = 1
  and sku_status_normal=1
  and grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','MX','VN')
  group by 1,2
  having sum(stock_on_hand)>0
  )
  where shopid not in 
  ( select distinct shop_id 
  FROM
  mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live
  WHERE
  grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','MX','VN')
  AND fulfilment_source = 'FULFILLED_BY_SHOPEE'
  AND tz_type = 'local'
  AND grass_date BETWEEN date_trunc('month', current_date - INTERVAL '1' DAY) - INTERVAL '1' MONTH
  AND current_date - INTERVAL '1' DAY
  AND is_placed = 1
  AND is_cb_shop = 1  
  )
  
 ),
 cbwh_history AS (
  -- CBWH history past 60 days 
  SELECT
  DISTINCT grass_region,
  grass_date,
  shop_id,
  warehouse_id    
  FROM
  regcbbi_general.cbwh_shop_history_v2
  WHERE
  grass_date BETWEEN current_date - INTERVAL '60' DAY
  AND current_date - INTERVAL '1' DAY
  AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','VN','MX')
   AND shop_id NOT IN (
  426379311,
  445279067,
  426377685,
  445275287,
  446091597,
  446089250
  ) --lovito
  union 
  select distinct grass_region, grass_date, shop_id, 'MXO' as warehouse_id 
  from sbs_mart.ads_retail_and_fbs_summary_mpsku_1d_mx
  where grass_date = current_date - interval'2'day
 

 

 ),
 

 -- table has retired
 -- cncb_userrole AS (
 --  -- latest info of CNCB userrole info
 --  SELECT
 --  userrole_name,
 --  team_group
 --  FROM
 --  regcbbi_general.shopee_regional_cb_team__cncb_user_role_match
 --  WHERE
 --  ingestion_timestamp = (
 --  SELECT
 --  max(ingestion_timestamp) AS latest_ingestion
 --  FROM
 --  regcbbi_general.shopee_regional_cb_team__cncb_user_role_match
 --  )
 -- ),
 

 -- seller_info AS (
 --  -- get the shop id and userrole_name of seller management team
 --  -- later to categorized the shops based on the userrole_name
 --  SELECT
 --  cast(sit.child_shopid AS bigint) AS shop_id,
 --  sit.gp_account_billing_country,
 --  sit.gp_account_billing_state_province,
 --  sit.gp_account_name,
 --  sit.gp_account_owner,
 --  sit.child_account_name
 -- merchant_name gp_account_name,
 --  -- ur.userrole_name,
 --  -- ur.team_group
 --  FROM
 --  cncbbi_general.shopee_cb_seller_profile_with_old_column sit
 --  --regbd_sf.cb__seller_index_tab sit
 -- -- LEFT JOIN cncb_userrole ur ON sit.child_account_owner_userrole_name = ur.userrole_name
 --  WHERE
 --  sit.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','VN','MX')
 -- ),
 shop_category AS (
  SELECT
  shop_id,
  shop_level1_global_be_category as shop_level1_category
  FROM
  mp_item.dws_shop_listing_td__reg_s0_live
  WHERE
  tz_type = 'local'
  AND grass_date = current_date - INTERVAL '1' DAY
  AND grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','VN','MX')
 ),
 sip_shops AS (
  -- get sip master and affiliated shops
  SELECT
  DISTINCT ms.shopid AS mst_shopid,
  ms.country AS mst_country,
  ms.cb_option,
  cast(sm.affi_shopid AS bigint) AS affi_shopid
  FROM
  marketplace.shopee_sip_db__mst_shop_tab__reg_daily_s0_live ms -- where grass_region in ('ID','MY','PH','TH','SG,'VN') 
  LEFT JOIN marketplace.shopee_sip_db__shop_map_tab__reg_daily_s0_live sm ON ms.shopid = sm.mst_shopid
  WHERE
  sm.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','VN','MX')
 ),
 

 seller_info as (
 

  SELECT
  DISTINCT shop_id
  , user_name as child_account_name
  ,cb_group as smt_group
  , merchant_name as gp_account_name
  , merchant_region as gp_account_billing_country
  , merchant_owner_email as gp_account_owner
  , merchant_organization_name as gp_account_owner_userrole_name
  , seller_id as ggp_account_id
  , seller_name as ggp_account_name
 

  FROM
  dev_regcbbi_general.cb_seller_profile_reg a
  WHERE
  grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG', 'VN', 'MX')
 

 )
 

 

 SELECT
  DISTINCT u.grass_region,
  /*Seller type categorization referenced SMT_userrole*/
  smt_group,
  -- coalesce(SMT_Group,SMT_Group_1,'') SMT_Group,
  /*why use grass region as warehouse_id*/
  u.grass_region AS warehouse_id,
  si.gp_account_name,
  si.gp_account_owner,
  u.shop_id,
  si.child_account_name,
  s.shop_level1_category,
  o.ord AS reference
 FROM
  cbwh_history u
  inner JOIN fbs_order o ON u.shop_id = o.shop_id
  LEFT JOIN seller_info si ON u.shop_id = si.shop_id
  LEFT JOIN shop_category s ON u.shop_id = s.shop_id
  --left join smt_group on u.shop_id = smt_group. shop_id
  --LEFT JOIN sip_shops ss ON u.shop_id = ss.affi_shopid
 where u.shop_id is not null
 ORDER BY
  reference DESC