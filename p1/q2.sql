insert into nexus.test_dc723a65f874e4c7141e2a629660de225175b3c6ca849a7b85777c1b86a30856_27c9d8900ec0ec2e89fdb1920eff6482 /**********************************
  Name: shop monitor daily
  Function: key metrics for WHS to monitor shops performnace
  Notes: created by resigned BI some time ago
  
  Date Developer Activity
  2022-03-29  Mingyu  exclude lovito shops
  2022-02-01  Hannah  fix data gap - raised by yuying
  2022-02-24  Hannah  add new fields - rebates
  2022-04-04  Hannah  need add MX, vn fbs shops
  2022-04-08  Hannah  update cfs / ads / views logic on item level
  2022-04-09  Hannah  test gitlab push pull
  2022-05-12  Hannah  add shops having stock_on_hand > 0
  2022-05-31  Hannah comment out deleted table regbida_userbehavior.shopee_bi_campaign_item
  2022-07-25  Hannah modify live sku logic and 
  
  **********************************/
WITH cb_fbs_shops AS (
  -- all cb shops have orders fulfilled by shopee last month (M-1 to D-1)
  SELECT 
    DISTINCT grass_region, 
    shop_id, 
    'having orders since M-1' shop_type 
  FROM 
    mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live 
  WHERE 
    grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    AND fulfilment_source = 'FULFILLED_BY_SHOPEE' 
    AND tz_type = 'local' 
    AND grass_date BETWEEN date_trunc(
      'month', current_date - INTERVAL '1' DAY
    ) - INTERVAL '1' MONTH 
    AND current_date - INTERVAL '1' DAY 
    AND is_placed = 1 
    AND is_cb_shop = 1 
    AND shop_id NOT IN (
      426379311, 445279067, 426377685, 445275287, 
      446091597, 446089250
    ) --lovito
  union 
  select 
    Distinct grass_region, 
    shopid as shop_id, 
    'No order since M-1' shop_type 
  from 
    (
      select 
        grass_region, 
        shopid, 
        sum(stock_on_hand) sellable_stock 
      from 
        sbs_mart.shopee_bd_sbs_mart_v2 
      where 
        grass_date = current_date - INTERVAL '1' DAY 
        and is_cb = 1 
        and sku_status_normal = 1 
        and grass_region IN (
          'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
        ) 
      group by 
        1, 
        2 
      having 
        sum(stock_on_hand)> 0
    ) 
  where 
    shopid not in (
      select 
        distinct shop_id 
      FROM 
        mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live 
      WHERE 
        grass_region IN (
          'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
        ) 
        AND fulfilment_source = 'FULFILLED_BY_SHOPEE' 
        AND tz_type = 'local' 
        AND grass_date BETWEEN date_trunc(
          'month', current_date - INTERVAL '1' DAY
        ) - INTERVAL '1' MONTH 
        AND current_date - INTERVAL '1' DAY 
        AND is_placed = 1 
        AND is_cb_shop = 1
    )
), 
-- PFF shops and items -- have inventory on 1st day of chosen time windows
-- sbs_mart https://docs.google.com/spreadsheets/d/1l6PwOkLHZxSnKWjqi-IkJ2LJ1slhkbZYu3Hn-COprWA/edit#gid=1387108429
item_info as (
  select 
    distinct cast(a.shop_id AS bigint) AS shop_id, 
    cast(a.item_id AS bigint) AS item_id, 
    case when stock > 0 then 1 else 0 end as whether_to_be_counted 
  from 
    sbs_mart.shopee_scbs_db__sku_tab__reg_daily_s0_live a 
    left join (
      
      /**
        select distinct shop_id, item_id, 
        sum(stock_on_hand_qty_1d) stock
        from 
        (
        select a.mp_sku_id,a.mt_sku_id, a.shop_id, whs_id, stock_on_hand_qty_1d,
        split(a.mp_sku_id, '_')[1] item_id
        from sbs_mart.ads_retail_and_fbs_summary_mpsku_1d_id a
        left join sbs_mart.ads_retail_and_fbs_summary_mtsku_whs_1d_id b
        on a.mt_sku_id = b.mt_sku_id 
        where 1=1
        and a.grass_date = current_date - INTERVAL '1' DAY
        and b.grass_date = current_date - INTERVAL '1' DAY
        )
        group by 1,2**/
      select 
        shop_id, 
        item_id, 
        sum(stock_on_hand) stock 
      from 
        (
          select 
            sku_id, 
            mtsku_id, 
            shopid shop_id, 
            whs_id, 
            stock_on_hand, 
            itemid item_id 
          from 
            sbs_mart.shopee_bd_sbs_mart_v2 
          where 
            1 = 1 
            and grass_date = current_date - INTERVAL '1' DAY
        ) 
      group by 
        1, 
        2
    ) b on a.shop_id = b.shop_id 
    and a.item_id = b.item_id 
    join regcbbi_general.cbwh_shop_history_v2 c on a.shop_id = c.shop_id -- shopee_scbs_db_sku_tab
  where 
    1 = 1 
    and a.item_status = 1 
    and a.fbs_tag = 1 
    and a.fbs_status = 1 
    and a.cb_option = 1 
    AND a.grass_schema = 'shopee_scbs_db' -- fbs_mode 0 pff, fff 1 （only from whs） shop level setting
    and c.pff_tag = 1 
    and c.grass_date = current_date - INTERVAL '1' DAY
), 
-- Hannah: Add distinct function, delete item_id, create a new subquery
pff_skus_fbs AS (
  -- PFF skus
  SELECT 
    distinct cast(shop_id AS bigint) AS shop_id, 
    fbs_tag 
  FROM 
    sbs_mart.shopee_scbs_db__sku_tab__reg_daily_s0_live 
  WHERE 
    fbs_tag = 1 --select current shops still have fbs with shopee
    and fbs_status = 1 
    AND grass_schema = 'shopee_scbs_db'
), 
pff_skus AS (
  -- PFF skus
  SELECT 
    distinct cast(a.shop_id AS bigint) AS shop_id, 
    cast(a.item_id AS bigint) AS item_id, 
    fbs_tag 
  FROM 
    sbs_mart.shopee_scbs_db__sku_tab__reg_daily_s0_live a --inner join item_with_stock b on cast(a.shop_id AS bigint) = b.shop_id and cast(b.item_id AS bigint) = b.item_id
  WHERE 
    fbs_tag = 1 
    and fbs_status = 1 
    AND grass_schema = 'shopee_scbs_db'
), 
pff_shops AS (
  -- PFF shops
  SELECT 
    distinct shop_id, 
    pff_tag 
  FROM 
    regcbbi_general.cbwh_shop_history_v2 
  WHERE 
    grass_date = current_date - INTERVAL '1' DAY
), 
-- ####################
-- orders
-- ####################
fbs_order_stats AS (
  -- statistics of D-1 order Fulfilled by Shopee
  SELECT 
    o.grass_region, 
    o.shop_id, 
    
    /*Number of CB orders fulfilled by Shopee*/
    sum(o.order_fraction) AS d1_cb_fbs_order, 
    
    /*Number of CB orders from FBS-enabled shops Fulfilled by Shopee.*/
    sum(o.order_fraction) filter (
      WHERE 
        s.pff_tag = 1
    ) AS d1_cb_pff_fbs_order, 
    sum(gmv_usd) AS d1_cb_fbs_gmv, 
    sum(item_amount) AS d1_cb_fbs_item_sold 
  FROM 
    mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o 
    /*detailed table about order status*/
    LEFT JOIN pff_shops s ON o.shop_id = s.shop_id 
    /*ensure is_cb_shop and fbs_tag. fbs_tag=1 means this shop is a pff shop. */
  WHERE 
    o.grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    AND o.tz_type = 'local' 
    AND o.grass_date = current_date - INTERVAL '1' DAY 
    AND o.is_placed = 1 
    AND o.fulfilment_source = 'FULFILLED_BY_SHOPEE' 
    AND o.is_cb_shop = 1 
  GROUP BY 
    o.grass_region, 
    o.shop_id
), 
cb_order_stats AS (
  -- statistics D-1 orders from FBS-enabled shops
  SELECT 
    o.grass_region, 
    o.shop_id, 
    
    /*all cb orders from FBS-enabled shop*/
    sum(order_fraction) d1_cb_order, 
    
    /*all cb orders from FBS-enabled shop but fulfilled by CB seller*/
    sum(order_fraction) filter (
      WHERE 
        s.pff_tag = 1 
        AND fulfilment_source = 'FULFILLED_BY_CB_SELLER'
    ) AS d1_sls_order, 
    
    /*all cb orders of FBS-enabled skus but fulfilled by CB seller*/
    sum(order_fraction) filter (
      WHERE 
        s.pff_tag = 1 
        AND t.fbs_tag = 1 
        AND fulfilment_source = 'FULFILLED_BY_CB_SELLER'
    ) AS d1_pff_sls_order 
  FROM 
    mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o 
    LEFT JOIN pff_shops s ON o.shop_id = s.shop_id -- Hannah: use new sub query to be left join
    LEFT JOIN pff_skus_fbs t ON t.shop_id = s.shop_id 
  WHERE 
    o.grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    AND o.tz_type = 'local' 
    AND o.grass_date = current_date - INTERVAL '1' DAY 
    AND o.is_placed = 1 
    AND o.is_cb_shop = 1 
  GROUP BY 
    o.grass_region, 
    o.shop_id
), 
-- ####################
-- campaign
-- ####################
campaign_items AS (
  SELECT 
    0 item_id, 
    0 start_time, 
    0 end_time --FROM  regbida_userbehavior.shopee_bi_campaign_item
    --WHERE grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','MX','VN')
    ), 
fbs_campaign_info AS (
  -- campaign info about FBS orders. model level
  SELECT 
    DISTINCT o.order_id, 
    o.shop_id, 
    o.item_id, 
    o.model_id, 
    o.level1_category, 
    o.order_fraction, 
    o.bundle_deal_id, 
    o.bundle_order_item_id, 
    o.add_on_deal_id, 
    o.is_add_on_sub_item, 
    o.group_id, 
    o.group_buy_deal_id, 
    o.grass_date, 
    o.grass_region, 
    i.whether_to_be_counted, 
    CASE WHEN b.item_id IS NOT NULL THEN 1 ELSE 0 END AS campaign_flag, 
    CASE WHEN o.item_promotion_source = 'flash_sale' THEN 1 ELSE 0 END AS flash_sale_flag 
  FROM 
    mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live o 
    LEFT JOIN campaign_items b ON o.item_id = b.item_id 
    AND o.create_timestamp BETWEEN b.start_time 
    AND b.end_time 
    left join item_info i on o.shop_id = i.shop_id 
    and o.item_id = i.item_id 
  WHERE 
    o.grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    AND o.fulfilment_source = 'FULFILLED_BY_SHOPEE' 
    AND o.is_cb_shop = 1 
    AND o.tz_type = 'local' 
    AND o.grass_date = current_date - INTERVAL '1' DAY 
    AND o.is_placed = 1
), 
fbs_split_by_campaign AS (
  -- order statistics about campaigns on shop level
  SELECT 
    grass_region, 
    shop_id, 
    
    /*number of orders without campaign*/
    sum(order_fraction) filter(
      WHERE 
        campaign_flag = 0 
        AND flash_sale_flag = 0
    ) AS d1_fbs_organic, 
    
    /*number of orders under campaign but not belong to flash sale*/
    sum(order_fraction) filter (
      WHERE 
        campaign_flag = 1 
        AND flash_sale_flag = 0
    ) AS d1_fbs_campaign, 
    
    /*number of orders of flash sale*/
    sum(order_fraction) filter (
      WHERE 
        flash_sale_flag = 1
    ) AS d1_fbs_cfs, 
    sum (order_fraction) filter (
      WHERE 
        flash_sale_flag = 1
    ) AS d1_cbwh_item_cfs 
  FROM 
    fbs_campaign_info 
  GROUP BY 
    1, 
    2
), 
-- ####################
-- items
-- ####################
item_with_stock as (
  select 
    distinct cast(shop_id as varchar) as shop_id, 
    cast(item_id as varchar) as item_id 
  from 
    (
      select 
        distinct shopid as shop_id, 
        itemid as item_id, 
        sku_id, 
        stock_on_hand 
      from 
        sbs_mart.shopee_bd_sbs_mart_v2 
      where 
        is_live = 1 
        and sku_status_normal = 1 
        and stock_on_hand > 0 
        and grass_region in (
          'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
        ) 
        and grass_date = current_date - interval '1' day 
      union 
        -- for mx, have to use v3 table, and due to time zone difference
      select 
        distinct shop_id, 
        mp_sku_item_id as item_id, 
        mp_sku_id as sku_id, 
        stock_on_hand_qty_1d as stock_on_hand 
      from 
        sbs_mart.ads_retail_and_fbs_summary_mtsku_whs_1d_mx a 
        left join sbs_mart.ads_retail_and_fbs_summary_mpsku_1d_mx b on a.mt_sku_id = b.mt_sku_id 
        and a.grass_region = b.grass_region 
      where 
        is_normal_status = 1 
        and is_live = 1 
        and stock_on_hand_qty_1d > 0 
        and is_cb = 1 
        and a.grass_date = current_date - interval '2' day 
        and b.grass_date = current_date - interval '2' day
    )
), 
live_cb_items AS (
  -- live cb items
  SELECT 
    DISTINCT grass_region, 
    a.shop_id, 
    a.item_id 
  FROM 
    mp_item.dim_item__reg_s0_live a 
  WHERE 
    STATUS = 1 
    AND shop_status = 1 
    AND seller_status = 1 
    AND (
      is_holiday_mode = 0 
      OR is_holiday_mode IS NULL
    ) 
    AND tz_type = 'local' 
    AND stock > 0 
    AND grass_date = current_date - INTERVAL '1' DAY 
    AND grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    AND is_cb_shop = 1
), 
sku_summary AS (
  -- skus statistics
  SELECT 
    i.grass_region, 
    i.shop_id, 
    count(DISTINCT i.item_id) AS total_live_sku_from_fbs_shops, 
    count(
      -- cbwh_live_sku need to have stock in whs
      DISTINCT CASE WHEN (
        
        /*PFF skus from PFF shops*/
        ps.pff_tag = 1 
        AND psu.item_id IS NOT NULL -- has fbs_tag=1
        and b.item_id is not null -- has stock in cbwh
        ) 
      /*pure whs skus having stock in cbwh*/
      OR (
        ps.pff_tag = 0 
        and b.item_id is not null
      ) THEN i.item_id 
      /*if NULL, not counted*/
      ELSE NULL END
    ) AS cb_wh_live_sku, 
    count(
      DISTINCT CASE 
      /*PFF live skus not FBS => SLS*/
      WHEN ps.pff_tag = 1 
      AND psu.item_id IS NULL THEN i.item_id ELSE NULL END
    ) AS cb_sls_live_sku 
  FROM 
    
    /*by inner joining the first 2 tables to get live skus from all FBS-enabled shop*/
    live_cb_items i 
    INNER JOIN cb_fbs_shops s ON s.shop_id = i.shop_id 
    LEFT JOIN pff_shops ps ON ps.shop_id = i.shop_id 
    LEFT JOIN pff_skus psu ON psu.item_id = i.item_id --need have stock in cbwh
    left join item_with_stock b on cast(i.shop_id as varchar) = b.shop_id 
    and cast(i.item_id as varchar) = b.item_id 
    /*LEFT JOIN sku_raw wh on i.item_id = wh.itemid and wh.shopid=i.shop_id and i.grass_region=wh.grass_region*/
  GROUP BY 
    1, 
    2
), 
-- ####################
-- flashsales
-- ####################
flashsales as (
  select 
    shop_id as shopid, 
    count(
      DISTINCT (item_id, start_timestamp)
    ) as slots 
  from 
    mp_promo.dim_flash_sale_sku__reg_s0_live 
  where 
    1 = 1 
    and grass_date = current_date - interval '1' day 
    and date(start_datetime) = current_date - interval '1' day 
    AND model_status_id = 1 
  group by 
    1
), 
slots_cnt AS (
  -- flash sale statistics for cbwh only
  SELECT 
    s.grass_region, 
    s.shop_id, 
    sum(slots) AS wh_cfs_slots 
  FROM 
    flashsales fs 
    INNER JOIN cb_fbs_shops s ON s.shop_id = fs.shopid 
  GROUP BY 
    1, 
    2
), 
-- cfs slot on item level
flashsales_1 as (
  select 
    a.shop_id as shopid, 
    count(
      DISTINCT (a.item_id, start_timestamp)
    ) as slots 
  from 
    mp_promo.dim_flash_sale_sku__reg_s0_live a 
    inner join item_info b on a.shop_id = b.shop_id 
    and a.item_id = b.item_id 
  where 
    1 = 1 
    and grass_date = current_date - interval '1' day 
    and date(start_datetime) = current_date - interval '1' day 
    AND model_status_id = 1 
    and whether_to_be_counted = 1 
  group by 
    1
), 
slots_cnt_1 AS (
  -- flash sale statistics for cbwh only
  SELECT 
    s.grass_region, 
    s.shop_id, 
    sum(slots) AS cbwh_item_cfs_slots 
  FROM 
    flashsales_1 fs 
    INNER JOIN cb_fbs_shops s ON s.shop_id = fs.shopid 
  GROUP BY 
    1, 
    2
), 
-- ####################
-- traffic
-- ####################
item_exposure AS (
  -- views for each shop
  SELECT 
    grass_region, 
    shop_id, 
    is_cross_border, 
    sum(pv_cnt_1d) AS views 
  FROM 
    mp_shopflow.dws_item_civ_1d__reg_s0_live 
  WHERE 
    tz_type = 'local' 
    AND grass_date = current_date - INTERVAL '1' DAY 
    AND grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
  GROUP BY 
    1, 
    2, 
    3
), 
traffic AS (
  -- traffic for each cbwh shop
  SELECT 
    e.grass_region, 
    e.shop_id, 
    1.00 * sum(views) AS d1_cbwh_view 
  FROM 
    item_exposure e 
    INNER JOIN cb_fbs_shops s ON s.shop_id = e.shop_id 
    AND s.grass_region = e.grass_region 
  where 
    e.is_cross_border = 1 --on v.grass_region = wh.grass_region and v.shop_id = wh.shopid
  GROUP BY 
    1, 
    2
), 
-- views on items+ shop level in CBWH
item_exposure_1 AS (
  SELECT 
    a.grass_region, 
    a.shop_id, 
    a.item_id, 
    is_cross_border, 
    sum(pv_cnt_1d) AS views 
  FROM 
    mp_shopflow.dws_item_civ_1d__reg_s0_live a 
    inner join item_info b on a.shop_id = b.shop_id 
    and a.item_id = b.item_id 
  WHERE 
    tz_type = 'local' 
    AND grass_date = current_date - INTERVAL '1' DAY 
    AND grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    and is_cross_border = 1 
    and whether_to_be_counted = 1 
  GROUP BY 
    1, 
    2, 
    3, 
    4
), 
traffic_1 AS (
  -- traffic for each cbwh shop on item level
  SELECT 
    e.grass_region, 
    e.shop_id, 
    1.000 * sum(views) AS d1_cbwh_item_view 
  FROM 
    item_exposure_1 e 
    INNER JOIN cb_fbs_shops s ON s.shop_id = e.shop_id 
    AND s.grass_region = e.grass_region 
  GROUP BY 
    1, 
    2
), 
inv AS (
  select 
    distinct grass_region, 
    shopid as shop_id, 
    sum(stock_on_hand) AS sellable_stock, 
    max(l60_purchasable_days) l60_purchasable_days, 
    sum(l60_gross_itemsold) l60_gross_itemsold 
  from 
    sbs_mart.shopee_bd_sbs_mart_v2 --shopee.shopee_bd_sbs_mart_v2
  where 
    grass_date = current_date - interval '1' day 
    and is_cb = 1 
    and is_live = 1 
    and sku_status_normal = 1 
    and grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
  group by 
    1, 
    2 
  union 
    -- for mx, have to use v3 table, and due to time zone difference
  select 
    distinct a.grass_region, 
    shop_id, 
    sum(stock_on_hand_qty_1d) as stock_on_hand, 
    max(0) l60_purchasable_days, 
    max(1) l60_gross_itemsold 
  from 
    sbs_mart.ads_retail_and_fbs_summary_mpsku_1d_mx a 
    left join sbs_mart.ads_retail_and_fbs_summary_mtsku_whs_1d_mx b on a.mt_sku_id = b.mt_sku_id 
    and a.grass_region = b.grass_region 
  where 
    is_normal_status = 1 
    and is_live = 1 
    and a.grass_date = current_date - interval '2' day 
    and b.grass_date = current_date - interval '2' day 
    and is_cb = 1 
  group by 
    1, 
    2
), 
-- net ads revenue = paid ads + paid ads expired 
ads_raw AS (
  select 
    grass_region, 
    shop_id, 
    paid_expenditure_w_expiry_amt_usd_1d + paid_expenditure_wo_expiry_amt_usd_1d as d1_ads_expense 
  from 
    (
      SELECT 
        a.grass_region, 
        a.shop_id, 
        sum(
          paid_expenditure_w_expiry_amt_usd_1d
        ) as paid_expenditure_w_expiry_amt_usd_1d, 
        sum(
          paid_expenditure_wo_expiry_amt_usd_1d
        ) as paid_expenditure_wo_expiry_amt_usd_1d 
      FROM 
        mp_paidads.ads_advertiser_mkt_1d__reg_s0_live a 
      WHERE 
        1 = 1 
        and a.grass_date = CURRENT_DATE - INTERVAL '1' DAY 
        AND a.tz_type = 'local' 
        AND a.grass_region IN (
          'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
        ) 
      GROUP BY 
        1, 
        2
    )
), 
ads_raw_1 as (
  SELECT 
    a.grass_region, 
    a.shop_id, 
    sum(order_cnt) as d1_ads_order 
  from 
    mp_paidads.ads_advertise_mkt_1d__reg_s0_live a 
  where 
    grass_date = CURRENT_DATE - INTERVAL '1' DAY 
    AND a.tz_type = 'local' 
    AND a.grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
  GROUP BY 
    1, 
    2
), 
ads as (
  select 
    a.grass_region, 
    b.shop_id, 
    d1_ads_expense, 
    d1_ads_order 
  from 
    ads_raw a 
    left join ads_raw_1 b on a.shop_id = b.shop_id
), 

/**
 gross ads revenue = paid ads + free ads 
 ads as (
 select a.grass_region, a.shop_id, 
 sum(order_cnt) d1_ads_order, 
 sum(ads_expenditure_amt_usd) d1_ads_expense
 from mp_paidads.ads_advertise_mkt_1d__reg_s0_live a
 where 1=1
 and grass_date = CURRENT_DATE - INTERVAL '2' DAY
 AND a.tz_type = 'local'
 AND a.grass_region IN ('ID', 'MY', 'PH', 'TH', 'SG','MX','VN')
 group by 1,2
 ),
 **/
-- net paid ads on item level
ads_1 as (
  select 
    b.grass_region, 
    b.shop_id, 
    cbwh_item_ads_expense, 
    cbwh_item_order_cnt 
  from 
    (
      select 
        a.grass_region, 
        a.shop_id, 
        sum(order_cnt) cbwh_item_order_cnt 
      from 
        mp_paidads.ads_advertise_mkt_1d__reg_s0_live a 
        inner join item_info b on a.shop_id = b.shop_id 
        and a.item_id = b.item_id 
      where 
        1 = 1 
        and grass_date = current_date - INTERVAL '1' DAY 
        AND a.tz_type = 'local' 
        AND a.grass_region IN (
          'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
        ) 
        and whether_to_be_counted = 1 
      group by 
        1, 
        2
    ) a 
    left join (
      select 
        a.grass_region, 
        a.shop_id, 
        sum(deduction_amt_usd) cbwh_item_ads_expense 
      from 
        mp_paidads.dwd_advertiser_deduction_di__reg_s0_live a 
        inner join item_info b on a.shop_id = b.shop_id 
        and a.item_id = b.item_id 
      where 
        1 = 1 
        and grass_date = current_date - INTERVAL '1' DAY 
        AND a.tz_type = 'local' 
        AND a.grass_region IN (
          'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
        ) 
        and whether_to_be_counted = 1 
      group by 
        1, 
        2
    ) b on a.grass_region = b.grass_region 
    and a.shop_id = b.shop_id
), 
cbwh_history AS (
  -- all cbwh shops from last 60 days
  SELECT 
    DISTINCT grass_region, 
    grass_date, 
    shop_id AS shopid, 
    shop_status, 
    holiday_mode_on -- last_login
  FROM 
    regcbbi_general.cbwh_shop_history_v2 
  WHERE 
    grass_date BETWEEN current_date - INTERVAL '60' DAY 
    AND current_date - INTERVAL '1' DAY 
    AND grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    )
), 
-- hannah: sellable days look at the past 60days, has order, and also not on holiday, last login in 7 days 
shops_w_order_60d AS (
  -- all cb shops has order last 60 days
  SELECT 
    DISTINCT grass_date, 
    shop_id 
  FROM 
    mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live 
  WHERE 
    grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    AND grass_date BETWEEN current_date - INTERVAL '60' DAY 
    AND current_date - INTERVAL '1' DAY 
    AND tz_type = 'local' 
    AND is_placed = 1 
    AND is_cb_shop = 1
), 
sellable_day AS (
  -- hannah: need to use sbs v2 table, using total stock on hand / purchase_days / l60 gross item sold
  -- get all the dates that the cb shops are either alive or have orders
  -- the cbwh filter is applied at the very end
  SELECT 
    DISTINCT coalesce(i.grass_date, o.grass_date) AS grass_date, 
    coalesce(i.shopid, o.shop_id) AS shopid, 
    IF(
      
      /*live cbwh shops*/
      (
        I.shop_status = 1 
        AND (
          holiday_mode_on = false 
          OR holiday_mode_on IS NULL
        ) --AND date(last_login) >= i.grass_date - INTERVAL '7' DAY
        ) 
      /*or any shop that has order*/
      OR O.grass_date IS NOT NULL, 
      1, 
      0
    ) AS purchasibility 
  FROM 
    cbwh_history I FULL 
    JOIN shops_w_order_60d O ON I.shopid = O.shop_id 
    AND I.grass_date = O.grass_date
), 
sellable AS (
  -- number of sellable days during past 60 days for each shop
  SELECT 
    shopid, 
    sum(purchasibility) AS sellable_days 
  FROM 
    sellable_day 
  GROUP BY 
    1
), 
-- ##################
-- ATP
-- ##################
-- hannah: comment out stock on hand, and also change it to join instead of inner join
sku_raw AS (
  --cb+local
  SELECT 
    DISTINCT grass_region, 
    shopid, 
    CAST(itemid AS BIGINT) AS itemid, 
    sku_id, 
    fe_stock 
  FROM 
    
    /*BI data for SBS related order, use this table to get stock_on_hand*/
    sbs_mart.shopee_bd_sbs_mart_v2 
  WHERE 
    grass_date = CURRENT_DATE - INTERVAL '1' DAY 
    AND grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) --hannah: comment out otherwise ATP = 1
    --AND stock_on_hand > 0
    AND sku_status_normal = 1 
    AND is_cb = 1
), 
live_models AS (
  -- all live models
  SELECT 
    DISTINCT grass_region, 
    shopid AS shop_id, 
    sku_id 
  FROM 
    sku_raw 
  WHERE 
    fe_stock > 0 
  UNION 
  SELECT 
    DISTINCT grass_region, 
    shop_id, 
    concat(
      CAST(item_id AS VARCHAR), 
      '_', 
      CAST(model_id AS VARCHAR)
    ) AS sku_id 
  FROM 
    mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live 
  WHERE 
    grass_region IN (
      'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
    ) 
    AND fulfilment_source = 'FULFILLED_BY_SHOPEE' 
    AND tz_type = 'local' 
    AND grass_date = CURRENT_DATE - INTERVAL '1' DAY 
    AND is_placed = 1 
    AND is_cb_shop = 1
), 
live_model_summary AS (
  SELECT 
    grass_region, 
    shop_id, 
    count(DISTINCT sku_id) AS live_sku_cnt 
  FROM 
    live_models 
  GROUP BY 
    1, 
    2
), 
replenish AS (
  SELECT 
    i.grass_region, 
    i.shop_id, 
    count(DISTINCT i.sku_id) AS to_replenish 
  FROM 
    (
      SELECT 
        grass_region, 
        shopid AS shop_id, 
        sku_id 
      FROM 
        sku_raw 
      WHERE 
        fe_stock = 0
    ) i 
    JOIN (
      SELECT 
        grass_region, 
        shop_id, 
        concat(
          cast(item_id AS VARCHAR), 
          '_', 
          cast(model_id AS VARCHAR)
        ) sku_id, 
        sum(order_fraction) l60d_orders 
      FROM 
        mp_order.dwd_order_item_place_pay_complete_di__reg_s0_live 
      WHERE 
        grass_region IN (
          'ID', 'MY', 'PH', 'TH', 'SG', 'MX', 'VN'
        ) 
        AND fulfilment_source = 'FULFILLED_BY_SHOPEE' 
        AND tz_type = 'local' 
        AND grass_date BETWEEN date_add('day', -60, current_date) 
        AND date_add('day', -1, current_date) 
        AND is_placed = 1 
        AND is_cb_shop = 1 
      GROUP BY 
        1, 
        2, 
        3
    ) o ON i.grass_region = o.grass_region 
    AND i.sku_id = o.sku_id 
    AND i.shop_id = o.shop_id 
    LEFT JOIN live_models l ON i.grass_region = l.grass_region 
    AND i.sku_id = l.sku_id 
    AND i.shop_id = l.shop_id 
  WHERE 
    l.sku_id IS NULL 
  GROUP BY 
    1, 
    2
), 
-- Hannah: add new metrics: item rebate, and voucher sponsored by shopee
order_tab AS (
  SELECT 
    grass_region, 
    date(
      from_unixtime(create_timestamp)
    ) as grass_date, 
    shop_id, 
    order_id, 
    item_id, 
    model_id, 
    gmv_usd, 
    item_amount, 
    order_be_status_id, 
    order_fraction, 
    if(is_returned_item = 1, 1, 0) as is_return, 
    item_rebate_by_shopee_amt_usd as item_rebate, 
    pv_rebate_by_shopee_amt_usd as pvoucher_rebate, 
    -- platoform voucher rebate by shopee
    sv_rebate_by_shopee_amt_usd as svoucher_rebate, 
    -- seller voucher rebate by shopee
    estimate_shipping_rebate_by_shopee_amt_usd as shipping_rebate, 
    -- why not actual shipping rebate
    coin_earn_cash_amt_usd as coin_rebate -- coin cashback provided by shopee is complex
  FROM 
    mp_order.dwd_order_item_all_ent_df__reg_s0_live 
  WHERE 
    grass_region IN (
      'SG', 'MY', 'ID', 'TH', 'PH', 'MX', 'VN'
    ) 
    AND grass_date >= current_date - interval '65' day 
    AND tz_type = 'local' 
    AND DATE(
      from_unixtime(create_timestamp)
    ) = CURRENT_DATE - INTERVAL '1' DAY 
    AND fulfilment_source IN (
      'FULFILLED_BY_SHOPEE', 'FULFILLED_BY_LOCAL_SELLER'
    ) 
    AND is_cb_shop = 1 
    AND is_bi_excluded = 0
), 
order_raw AS (
  SELECT 
    DISTINCT *, 
    count(is_return) OVER (PARTITION BY order_id) AS total_items, 
    sum(is_return) OVER (PARTITION BY order_id) AS return_items 
  FROM 
    order_tab
), 
order_raw_v2 AS (
  SELECT 
    *, 
    IF(
      order_be_status_id IN (1, 2, 3, 4, 11, 12, 13, 14, 15) 
      AND return_items = 0, 
      1, 
      0
    ) AS is_net 
  FROM 
    order_raw
), 
final_order_info AS (
  SELECT 
    grass_region, 
    grass_date, 
    shop_id, 
    sum(item_rebate) as item_rebate, 
    sum(pvoucher_rebate) as pvoucher_rebate, 
    sum(svoucher_rebate) as svoucher_rebate, 
    sum(shipping_rebate) as shipping_rebate --sum(coin_rebate) as coin_rebate
  FROM 
    order_raw_v2 
  WHERE 
    is_net = 1 -- get net orders
  GROUP BY 
    1, 
    2, 
    3 
  ORDER BY 
    2, 
    3, 
    4, 
    5
) -- ##################
-- Combined
-- ##################
SELECT 
  DISTINCT s.grass_region, 
  s.shop_id, 
  fos.d1_cb_fbs_order, 
  -- sls = non warehouse skus
  -- pff 
  -- fbs Order definition
  cos.d1_sls_order, 
  fos.d1_cb_pff_fbs_order, 
  try(
    fos.d1_cb_pff_fbs_order * 1.0000 /(
      coalesce(fos.d1_cb_pff_fbs_order, 0) + coalesce(cos.d1_pff_sls_order, 0)
    )
  ) AS pff_order_pct, 
  fos.d1_cb_fbs_gmv, 
  try(
    fos.d1_cb_fbs_gmv * 1.0000 / fos.d1_cb_fbs_order
  ) AS d1_cb_fbs_abs, 
  fos.d1_cb_fbs_item_sold, 
  fos.d1_cb_fbs_item_sold * 1.0000 / fos.d1_cb_fbs_order AS d1_cb_fbs_item_per_order, 
  --1.0000*d1_cb_fbs_gmv/d1_cb_fbs_order as d1_cb_fbs_abs,
  ss.cb_wh_live_sku, 
  1.0000 * ss.cb_wh_live_sku / ss.total_live_sku_from_fbs_shops AS cb_wh_live_sku_pctg, 
  ss.cb_sls_live_sku, 
  try(
    1.0000 * cos.d1_cb_order / t.d1_cbwh_view
  ) AS cb_fbs_cr, 
  t.d1_cbwh_view total_d1_views, 
  --1.0000*d1_cb_order/d1_cb_view as cb_cr, 1.0000*d1_fbs_order/d1_country_view as total_fbs_cr,
  v.sellable_stock, 
  round(
    try(
      (
        coalesce(v.sellable_stock, 0)
      ) * v.l60_purchasable_days / nullif(v.l60_gross_itemsold, 0)
    ), 
    2
  ) AS coverage_days, 
  c.d1_fbs_organic, 
  c.d1_fbs_campaign, 
  c.d1_fbs_cfs, 
  --total_d1_cfs
  sc.wh_cfs_slots total_d1_cfs_slots, 
  ads.d1_ads_expense total_d1_ads_expense, 
  ads.d1_ads_order total_d1_ads_orders, 
  -- a.apt_avg, live_sku / (live_sku + replenish)
  try(
    coalesce(1.0000 * lms.live_sku_cnt, 0) / nullif(
      coalesce(1.0000 * lms.live_sku_cnt, 0) + coalesce(1.0000 * r.to_replenish, 0), 
      0
    )
  ) AS atp, 
  rebate_voucher.item_rebate, 
  rebate_voucher.pvoucher_rebate, 
  rebate_voucher.svoucher_rebate, 
  rebate_voucher.shipping_rebate, 
  -- cbwh items only 
  s1.cbwh_item_cfs_slots fbs_d1_cfs_slot, 
  --c.d1_cbwh_item_cfs fbs_d1_cfs,
  t1.d1_cbwh_item_view fbs_d1_views, 
  ad1.cbwh_item_ads_expense fbs_d1_ads_expense, 
  ad1.cbwh_item_order_cnt fbs_d1_ads_order, 
  s.shop_type 
FROM 
  cb_fbs_shops s 
  LEFT JOIN inv v ON s.grass_region = v.grass_region 
  AND s.shop_id = v.shop_id 
  LEFT JOIN fbs_order_stats fos ON s.grass_region = fos.grass_region 
  AND s.shop_id = fos.shop_id 
  LEFT JOIN cb_order_stats cos ON s.grass_region = cos.grass_region 
  AND s.shop_id = cos.shop_id 
  LEFT JOIN fbs_split_by_campaign c ON s.grass_region = c.grass_region 
  AND s.shop_id = c.shop_id 
  LEFT JOIN sku_summary ss ON s.grass_region = ss.grass_region 
  AND s.shop_id = ss.shop_id 
  LEFT JOIN slots_cnt sc ON s.grass_region = sc.grass_region 
  AND s.shop_id = sc.shop_id 
  LEFT JOIN traffic t ON s.grass_region = t.grass_region 
  AND s.shop_id = t.shop_id 
  LEFT JOIN ads ON s.grass_region = ads.grass_region 
  AND s.shop_id = ads.shop_id 
  LEFT JOIN live_model_summary lms ON s.shop_id = lms.shop_id 
  LEFT JOIN replenish r ON s.shop_id = r.shop_id 
  left join final_order_info rebate_voucher on s.grass_region = rebate_voucher.grass_region 
  AND s.shop_id = rebate_voucher.shop_id 
  left join traffic_1 t1 on s.grass_region = t1.grass_region 
  and s.shop_id = t1.shop_id 
  left join slots_cnt_1 s1 on s.grass_region = s1.grass_region 
  and s.shop_id = s1.shop_id 
  left join ads_1 ad1 on s.grass_region = ad1.grass_region 
  and s.shop_id = ad1.shop_id 
ORDER BY 
  1, 
  3 DESC
